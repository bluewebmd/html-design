import {submenu} from "./js/submenu";
import {quickBasket} from "./js/quick-basket";
import {profileSubmenu} from "./js/profile_submenu";
import {video} from "./js/video";
import {languages} from "./js/languages";
import {checkout} from "./js/checkout";
import profile from "./js/profile";


submenu.init();
video.init();
languages.init();
profileSubmenu.init();
quickBasket.init();
checkout.init();
profile.init();
