require('es6-promise').polyfill();
import Vue from "vue";
import Vuex from "vuex";
import axios from "axios"

Vue.use(Vuex);

if (typeof builder_object === "undefined") {
  window.img_url = "/img/builder/";
} else {
  window.img_url = builder_object.absolute_path;
}

export const store = new Vuex.Store({
  state: {
    sizes: [],
    measures: {
      body: [
        { name: "Body Length", value: 27 },
        { name: "Shoulder", value: 16.75 },
        { name: "Chest circumference", value: 38 },
        { name: "Waist circumference", value: 35.5 },
        { name: "Hips circumference", value: 38 }
      ],
      neck: [{ name: "Neck Circumference", value: 14 }],
      sleeves: [
        { name: "Sleeve", value: 24 },
        { name: "Elbow circumference", value: 12 },
        { name: "Cuffs circumference", value: 9 }
      ],
      shoulders: 'shoulders-normal'
    },
    default_measures: {
      body: [
        { name: "Body Length", value: 27 },
        { name: "Shoulder", value: 16.75 },
        { name: "Chest circumference", value: 38 },
        { name: "Waist circumference", value: 35.5 },
        { name: "Hips circumference", value: 38 }
      ],
      neck: [{ name: "Neck circumference", value: 14 }],
      sleeves: [
        { name: "Sleeve", value: 24 },
        { name: "Elbow circumference", value: 12 },
        { name: "Cuffs circumference", value: 9 }
      ],
      shoulders: 'shoulders-normal'
    },
    parts: {
      color: {name: "White Twill", type: 'single', id: 1},
      front: "No Placket",
      cuffs: "Single Cuff",
      size: {
        neck: {
          number: 15,
          measure: "inches"
        },
        body_shape: 1,
        height: "",
        fit: "fit",
        sleeves: 24,
        customSleeves: null,
        shoulders: null
      }
    },

    fabrics: {
      colors: [
        // {
        //     img: window.img_url + "fabrics/1.jpg",
        //     name: 'White poplin',
        //     id: 1,
        //     meta: 'Illuminous, crisp white fabric. A must have in your TOR shirt collection.'
        // },
        {
            img: window.img_url + "fabrics/1.jpg",
            name: 'White Twill',
            id: 1,
            meta: 'The perfect white dress shirt. Smooth and crisp with just a touch of shine.'
        },
        {
            img: window.img_url + "fabrics/2.jpg",
            name: 'Light Blue Fine Twill',
            id: 2,
            meta: 'Beautiful colour, smooth feel and luxurious look.'
        },
        {
            img: window.img_url + "fabrics/3.jpg",
            name: 'Blue Fine Twill',
            id: 3,
            meta: 'Add a stylish blue to your TOR shirt collection.'
        },
         {
            img: window.img_url + "fabrics/10.jpg",
            name: 'Light Pink Twill',
            id: 10,
            meta: 'The classic English pink twill. This lightweight fabric wears perfectly with a dark blue or dark grey suit.'
        }

      ],
      textures: [
          {
            img: window.img_url + "fabrics/5.jpg",
            name: 'Blue Micro Stripe',
            id: 5,
            meta: 'Tasteful, lightweight, lustrous fabric. Perfect with a dark red tie.'
        },
        {
            img: window.img_url + "fabrics/6.jpg",
            name: 'Blue Fine Stripe',
            id: 6,
            meta: 'Casual and impeccable look. Smooth, comfortable, lightweight (but not transparent) premium fabric.'
        },
        {
            img: window.img_url + "fabrics/8.jpg",
            name: 'Lavendar Chequered',
            id: 8,
            meta: 'If purple is your colour, this medium weight fabric is a perfect addition to your capsule collection.'
        }
      ],
    },

    selectedSizeName: "",
    selectedSize: "",

    shirt: {
      folder: window.img_url + "shirts/",
      fileName: "1 - single - no-placket",
      ext: ".png"
    },

    quiz: false,

    guards: {
      isLogged: false,

      size: {
        setFabric: false,
        setCuffs: false,
        setFront: false,
        setNeck: false,
        setHeight: false,
        setBody: false,
        setFit: false,
        setSleeves: false,
        setShoulders: false
      }
    }
  },
  getters: {
    getParts:           state => state.parts,
    getMeasures:        state => state.measures,
    getDefaultMeasures: state => state.default_measures,
    getShirtLink:       state => state.shirt.folder + state.shirt.fileName + state.shirt.ext,
    getFabrics:         state => state.fabrics,
    getSizes:           state => state.sizes,
    getLogStatus:       state => state.guards.isLogged,
    getSelectedSize:    state => state.selectedSize,
    getSelectedSizeName: state => state.selectedSizeName,
    getFit:             state => state.parts.size.fit,
    getNeck:            state => state.parts.size.neck.number,
    getBody:            state => state.parts.size.body_shape,
    getHeight:          state => state.parts.size.height,
    getShoulders:       state => state.parts.size.shoulders
  },
  mutations: {
    updateColor: (state, color) => {
      state.parts.color.name = color.name;
      state.parts.color.type = color.type;
      state.parts.color.id = color.id;
      state.guards.size.setFabric = true;

      let colorUrl = state.shirt.fileName.split(" - ");
      colorUrl[0] = color.id;
      state.shirt.fileName = colorUrl.join(" - ");
    },
    updateFront: (state, front) => {
      state.parts.front = front;
      state.guards.size.setFront = true;

      let frontUrl = state.shirt.fileName.split(" - ");
      frontUrl[2] = front.toLowerCase().replace(" ", "-");
      state.shirt.fileName = frontUrl.join(" - ");
    },
    updateCuffs: (state, cuffs) => {
      state.parts.cuffs = cuffs;
      state.guards.size.setCuffs = true;

      let cuffsUrl = state.shirt.fileName.split(" - ");
      cuffsUrl[1] = cuffs
        .split(" ")[0]
        .toLowerCase()
        .replace(" ", "-");
      state.shirt.fileName = cuffsUrl.join(" - ");
    },

    updateNeck: (state, neck) => {
      state.parts.size.neck.number = neck.number;
      state.parts.size.neck.measure = neck.measure;
      state.guards.size.setNeck = true;
    },
    updateHeight: (state, height) => {
      state.parts.size.height = parseInt(height);
      state.guards.size.setHeight = true;
    },
    updateBodyShape: (state, shape) => {
      state.parts.size.body_shape = shape;
      state.guards.size.setBody = true;
    },
    updateFit: (state, fit) => {
      state.parts.size.fit = fit;
      state.guards.size.setFit = true;
    },
    updateShoulders: (state, shoulders) => {
      state.parts.size.shoulders = shoulders;
      state.guards.size.setShoulders = true;
    },
    updateSleeves: (state, sleeves) => {
      state.measures.sleeves[0].value = sleeves;
      state.parts.size.sleeves = sleeves;
      state.guards.size.setSleeves = true;
    },
    updateCustomSleeves: (state, sleeves) => {
      state.parts.size.customSleeves = sleeves;
      state.parts.size.sleeves = sleeves;
      if (sleeves) {
        state.guards.size.setSleeves = true;
      }
    },
    updateMeasures: (state, measures) => {
      state.measures = measures;
    },

    changeSizeName: (state, size) => {
      state.selectedSizeName = size;
    },

    changeSize: (state, size) => {
      state.selectedSize = size;

      localStorage.setItem("currentSize", JSON.stringify(size));
    },

    addSize: (state, size) => {
      state.sizes.push(size);
    },

    updateSizes: (state, sizes) => {
      state.sizes = sizes;
    },

    editSize: (state, size) => {
      state.sizes.forEach((el, index) => {
        if(el.name === size.name) {
          state.sizes[index] = size;
        }
    });
    },

    toggleQuiz: state => {
      state.quiz = true
    },
    updateLogged: (state, status) => {
      state.guards.isLogged = status
    }
  },
  actions: {
    addSize({commit, state}, size) {

      var params = new FormData();
      params.append("action", builder_object.add_size.action);
      params.append("nonce", builder_object.add_size.nonce);
      params.append("sizeName", size.name);
      params.append("measures", JSON.stringify(size.measures));
      params.append("parts", JSON.stringify(size.parts));
      if (state.guards.isLogged) {
        axios
        .post(builder_object.ajax_url, params)
        .then((response) => {

          if (response.data.success) {
            commit('addSize', size);
            //Event.fire('sizesUpdated');
          }

        })
        .catch((error) => {
          console.log(error);
        });
      } else {
        commit('addSize', size);
      }

    }
  }
});
