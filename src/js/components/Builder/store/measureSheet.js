export default {
  fit: {
    14: {
      chest: 38,
      waist: {
        default: 35.5,
        medium: 35.5,
        significant: 35.5
      },
      from_back: -1,
      shoulder: 16.75,
      hips: 38,
      length: {
        less: 27,
        more: 27.5
      },
      sleeve: {
        less: 22.5,
        more: 24
      },
      elbow: 12,
      cuffs: 9
    },
    14.5: {
      chest: 39.5,
      waist: {
        default: 36.5,
        medium: 36.5,
        significant: 36.5
      },
      from_back: -1,
      shoulder: 17.25,
      hips: 38,
      length: {
        less: 27.5,
        more: 28
      },
      sleeve: {
        less: 23,
        more: 24.5
      },
      elbow: 12.5,
      cuffs: 9.25
    },
    15: {
      chest: 40.5,
      waist: {
        default: 37,
        medium: 37,
        significant: 37.5
      },
      from_back: -1,
      shoulder: 17.75,
      hips: 39.5,
      length: {
        less: 28,
        more: 28.5
      },
      sleeve: {
        less: 23.25,
        more: 24.5
      },
      elbow: 13,
      cuffs: 9.5
    },
    15.5: {
      chest: 42,
      waist: {
        default: 38,
        medium: 38,
        significant: 38.5
      },
      from_back: -1,
      shoulder: 18,
      hips: 40.5,
      length: {
        less: 28.5,
        more: 29
      },
      sleeve: {
        less: 23.5,
        more: 25
      },
      elbow: 13.5,
      cuffs: 9.5
    },
    15.75: {
      chest: 43.5,
      waist: {
        default: 39.5,
        medium: 39.5,
        significant: 40
      },
      from_back: -1,
      shoulder: 18.25,
      hips: 43,
      length: {
        less: 28.5,
        more: 29.5
      },
      sleeve: {
        less: 23.5,
        more: 25
      },
      elbow: 13.75,
      cuffs: 9.75
    },
    16: {
      chest: 45,
      waist: {
        default: 41.0,
        medium: 41.5,
        significant: 41.75
      },
      from_back: -1.5,
      shoulder: 18.25,
      hips: 44.5,
      length: {
        less: 29,
        more: 30
      },
      sleeve: {
        less: 23.5,
        more: 25.5
      },
      elbow: 14,
      cuffs: 10
    },
    16.5: {
      chest: 46,
      waist: {
        default: 42,
        medium: 42.5,
        significant: 42.75
      },
      from_back: -1.5,
      shoulder: 18.5,
      hips: 45,
      length: {
        less: 29.5,
        more: 30.5
      },
      sleeve: {
        less: 24,
        more: 26
      },
      elbow: 14.5,
      cuffs: 10
    },
    17: {
      chest: 47,
      waist: {
        default: 43,
        medium: 43.5,
        significant: 43.75
      },
      from_back: -1.5,
      shoulder: 18.75,
      hips: 46,
      length: {
        less: 29.5,
        more: 31
      },
      sleeve: {
        less: 24.5,
        more: 26
      },
      elbow: 15,
      cuffs: 10.5
    },
    17.5: {
      chest: 48.5,
      waist: {
        default: 44,
        medium: 44.75,
        significant: 45
      },
      from_back: -1.5,
      shoulder: 18.75,
      hips: 47.5,
      length: {
        less: 29.5,
        more: 31.5
      },
      sleeve: {
        less: 24.5,
        more: 26
      },
      elbow: 15.5,
      cuffs: 10.5
    },
    17.75: {
      chest: 50.5,
      waist: {
        default: 47,
        medium: 47.75,
        significant: 48
      },
      from_back: -1.5,
      shoulder: 19.5,
      hips: 49,
      length: {
        less: 29.5,
        more: 31.5
      },
      sleeve: {
        less: 25,
        more: 26.25
      },
      elbow: 16,
      cuffs: 10.75
    },
    18: {
      chest: 53.5,
      waist: {
        default: 49.5,
        medium: 50.25,
        significant: 50.5
      },
      from_back: -1.5,
      shoulder: 20,
      hips: 51.5,
      length: {
        less: 30.5,
        more: 31.5
      },
      sleeve: {
        less: 25.5,
        more: 26.5
      },
      elbow: 16.5,
      cuffs: 10.75
    },
    18.5: {
      chest: 55,
      waist: {
        default: 51,
        medium: 51.75,
        significant: 52
      },
      from_back: -1.5,
      shoulder: 20.5,
      hips: 53.5,
      length: {
        less: 31,
        more: 31.5
      },
      sleeve: {
        less: 25.5,
        more: 26.5
      },
      elbow: 17,
      cuffs: 10.75
    }
  },
  slim: {
    14: {
      chest: 37,
      waist: {
        default: 34.5,
        medium: 34.5,
        significant: 34.5
      },
      from_back: -1,
      shoulder: 16.75,
      hips: 37,
      length: {
        less: 27,
        more: 27.5
      },
      sleeve: {
        less: 22.5,
        more: 24
      },
      elbow: 12,
      cuffs: 9
    },
    14.5: {
      chest: 38.5,
      waist: {
        default: 35.5,
        medium: 35.5,
        significant: 35.5
      },
      from_back: -1,
      shoulder: 17.25,
      hips: 37,
      length: {
        less: 27.5,
        more: 28
      },
      sleeve: {
        less: 23,
        more: 24.5
      },
      elbow: 12.5,
      cuffs: 9.25
    },
    15: {
      chest: 39.5,
      waist: {
        default: 36,
        medium: 36,
        significant: 36
      },
      from_back: -1,
      shoulder: 17.75,
      hips: 38.5,
      length: {
        less: 28,
        more: 28.5
      },
      sleeve: {
        less: 23.25,
        more: 24.5
      },
      elbow: 13,
      cuffs: 9.5
    },
    15.5: {
      chest: 41,
      waist: {
        default: 37,
        medium: 37,
        significant: 37
      },
      from_back: -1,
      shoulder: 18,
      hips: 39.5,
      length: {
        less: 28.5,
        more: 29
      },
      sleeve: {
        less: 23.5,
        more: 25
      },
      elbow: 13.5,
      cuffs: 9.5
    },
    15.75: {
      chest: 42.5,
      waist: {
        default: 38.5,
        medium: 38.5,
        significant: 38.5
      },
      from_back: -1,
      shoulder: 18.25,
      hips: 42,
      length: {
        less: 28.5,
        more: 29.5
      },
      sleeve: {
        less: 23.5,
        more: 25
      },
      elbow: 13.75,
      cuffs: 9.75
    },
    16: {
      chest: 44,
      waist: {
        default: 40,
        medium: 40,
        significant: 40.5
      },
      from_back: -1.5,
      shoulder: 18.25,
      hips: 43.5,
      length: {
        less: 29,
        more: 30
      },
      sleeve: {
        less: 23.5,
        more: 25.5
      },
      elbow: 14,
      cuffs: 10
    },
    16.5: {
      chest: 45,
      waist: {
        default: 41,
        medium: 41,
        significant: 41.5
      },
      from_back: -1.5,
      shoulder: 18.5,
      hips: 44,
      length: {
        less: 29.5,
        more: 30.5
      },
      sleeve: {
        less: 24,
        more: 26
      },
      elbow: 14.5,
      cuffs: 10
    },
    17: {
      chest: 46,
      waist: {
        default: 42,
        medium: 42,
        significant: 42.5
      },
      from_back: -1.5,
      shoulder: 18.75,
      hips: 45,
      length: {
        less: 29.5,
        more: 31
      },
      sleeve: {
        less: 24.5,
        more: 26
      },
      elbow: 15,
      cuffs: 10.5
    },
    17.5: {
      chest: 47.5,
      waist: {
        default: 43,
        medium: 43,
        significant: 43.5
      },
      from_back: -1.5,
      shoulder: 18.75,
      hips: 46.5,
      length: {
        less: 29.5,
        more: 31.5
      },
      sleeve: {
        less: 24.5,
        more: 26
      },
      elbow: 15.5,
      cuffs: 10.5
    },
    17.75: {
      chest: 49.5,
      waist: {
        default: 46,
        medium: 46,
        significant: 46.75
      },
      from_back: -1.5,
      shoulder: 19.5,
      hips: 48,
      length: {
        less: 29.5,
        more: 31.5
      },
      sleeve: {
        less: 25,
        more: 26.25
      },
      elbow: 16,
      cuffs: 10.75
    },
    18: {
      chest: 52.5,
      waist: {
        default: 48.5,
        medium: 48.5,
        significant: 49.25
      },
      from_back: -1.5,
      shoulder: 20,
      hips: 50.5,
      length: {
        less: 30.5,
        more: 31.5
      },
      sleeve: {
        less: 25.5,
        more: 26.5
      },
      elbow: 16.5,
      cuffs: 10.75
    },
    18.5: {
      chest: 54,
      waist: {
        default: 50,
        medium: 50,
        significant: 50.75
      },
      from_back: -1.5,
      shoulder: 20.5,
      hips: 52.5,
      length: {
        less: 31,
        more: 31.5
      },
      sleeve: {
        less: 25.5,
        more: 26.5
      },
      elbow: 17,
      cuffs: 10.75
    }
  }
};
