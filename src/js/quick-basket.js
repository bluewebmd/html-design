let quickBasket = (function($) {
  let init = function() {
    setUpListeners();
  };

  let setUpListeners = function() {
    // toggle small-cart
    $(".user__cart-link").click(function(e) {
      $(".quick-basket-wrap").toggleClass("active");
      e.stopPropagation();
      $(".has-submenu")
        .not($(this))
        .removeClass("active");
      $(this).toggleClass("active");
      $(this)
        .find(".submenu")
        .stop()
        .slideToggle();
    });
    $(".quick-basket-wrap").click(function(e) {
      e.stopPropagation();
      if ($(e.target).hasClass("quick-basket-wrap")) {
        $(".quick-basket-wrap").toggleClass("active");
      }
    });
  
    $("body").click(function(){
      $(".quick-basket-wrap").removeClass("active");
    });
  };

  return {
    init: init
  };
})(jQuery);

module.exports = {
  quickBasket
};
