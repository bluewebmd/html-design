let checkout = (function ($) {
  let init = function () {
    setUpListeners();
  };
  
  let setUpListeners = function () {
    $(".summary__count").click(function () {
      $(this).toggleClass('active');
      $('.summary__list-wrap>div:gt(0)').slideToggle();
    });
  };
  
  return {
    init: init
  };
})(jQuery);

module.exports = {
  checkout
};


