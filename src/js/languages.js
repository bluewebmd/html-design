let languages = (function($) {
  function language(data) {
    $("#form-language input[name='code']").val(data.attr("class"));
    $("#form-language").submit();
  }

  let init = function() {
    setUpListeners();
  };

  let setUpListeners = function() {
    $("#form-language li").click(function() {
      language($(this));
    });
  };

  return {
    init: init
  };
})(jQuery);

module.exports = {
  languages
};
