let submenu = (function($) {
  const submenu = $(".submenu");
  const submenu_bg = $(".submenu::before");

  const hasSubmenu = $(".has-submenu");

  const loginPopup = $(".builder-popup__bg");

  const loginButton = $(".login_button");

  const loginForm = $(".builder-popup--login");

  const loginLink = $(".builder-login__nav-link");

  const loginRegisterLink = $(".builder-login__nav-link--register");

  // loginLink    = $(".builder-popup--login"),

  const loginResetButton = $(".builder-login__reset");

  const loginResetForm = $(".builder-popup--reset");

  const loginRegister = $(".builder-popup--register");

  const headerHeight = $('aside.sidebar').first().height();

  let init = function() {
    setUpListeners();
    submenu_bg.height(submenu.height())
  };

  function closeSubmenu() {
    hasSubmenu.removeClass("active");
    submenu.slideUp();
  }

  function showSubmenu(e) {
    if ($(window).width() < 992) {
      submenu.slideUp();
      e.stopPropagation();
      hasSubmenu.not($(this)).removeClass("active");
      $(this).toggleClass("active");
      $(this)
        .find(".submenu")
        .stop()
        .slideToggle();
    }
  }

  function showLoginPopup(e) {
    loginPopup.removeClass("hidden");
  }

  function hideLoginPopup(e) {
    if ($(e.target).is(".builder-popup__bg")) {
      loginPopup.addClass("hidden");
    }
  }

  // function resizeSubMenu(e) {
  //   if (window.innerWidth > 990) {
  //     submenu.each(function( index ) {
  //       if (headerHeight <= 800) {
  //         $(this).height(headerHeight);
  //       } else {
  //         $(this).height(800);
  //       }
  //     });
  //   }
  // }

  function goToLogin(e) {
    loginForm.show();
    loginResetForm.hide();
    loginRegister.hide();
  }

  function goToReset(e) {
    loginForm.hide();
    loginResetForm.show();
  }

  function goToRegister(e) {
    loginForm.hide();
    loginRegister.show();
  }

  let setUpListeners = function() {
    hasSubmenu.click({ e: this }, showSubmenu);
    loginButton.on("click", showLoginPopup);
    loginPopup.on("click", { e: this }, hideLoginPopup);
    loginResetButton.click(goToReset);
    loginLink.click(goToLogin);
    loginRegisterLink.click(goToRegister);

    $(document)
      .keyup(function(e) {
        if ($(window).width() < 992) {
          if (e.keyCode === 27) {
            closeSubmenu();
          }
        }
      })
      .click(function() {
        if ($(window).width() < 992) {
          closeSubmenu();
        }
      });
  };

  return {
    init: init
  };
})(jQuery);

module.exports = {
  submenu
};
