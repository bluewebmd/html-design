import Builder from "./components/Builder/Builder.vue";
import VueRouter from "vue-router";
import Vue from "vue";

import { routes } from "./router";
import { store } from "./components/Builder/store/store";

Vue.use(VueRouter);
const router = new VueRouter({
  relative: true,
  base: "/shirt-builder.html",
  routes
});

if (typeof builder_object == 'undefined') {
  window.builder_object = {"product_id":"203","currencies":"SGD,USD,EUR,GBP,SEK","product_price":"SGD\u00a0109.00 - SGD\u00a0149.00","pattern_range_price":"SGD\u00a0129.00","pattern_range_price_text":"SGD\u00a0109.00 - SGD\u00a0129.00","premium_range_price":"SGD\u00a0149.00","premium_range_price_text":"SGD\u00a0126.00 - SGD\u00a0149.00","absolute_path":"https:\/\/torshirts.com\/wp-content\/themes\/tor\/assets\/img\/builder\/","order_data":"","ajax_url":"https:\/\/torshirts.com\/wp-admin\/admin-ajax.php","action":"add_to_cart","nonce":"f1ce06897b","size_action":"create_size","login":{"text":"","action":"ajax_login","nonce":"e9ea301e45"},"reset":{"text":"","action":"ajax_reset_pass","nonce":"c9ad5ab3d1"},"register":{"text":"","action":"ajax_register","nonce":"ba0426737e"},"user_sizes":{"action":"get_user_sizes","nonce":"81c88858b6"},"add_size":{"action":"add_size","nonce":"85cb21132f"},"get_sizes":{"action":"get_sizes","nonce":"9062f7a873"}};
}

if (typeof main_ajax_object == 'undefined') {
  window.main_ajax_object = {"ajax_url":"http:\/\/tor.tmdigi.com\/wp-admin\/admin-ajax.php","is_logged":"0","profile_url":"http:\/\/tor.tmdigi.com\/profile\/","remove_from_cart":{"action":"tor_remove_from_cart","nonce":"ce704fd72c"},"update_quantity":{"action":"tor_update_quantity","nonce":"d56f88f752"}};
}


window.Event = new class {
  constructor() {
    this.vue = new Vue();
  }

  fire(event, data = null) {
    this.vue.$emit(event, data);
  }

  listen(event, callback) {
    this.vue.$on(event, callback);
  }
}();

const app = new Vue({
  el: "#shirt-builder",
  router,
  store,
  render: h => h(Builder)
});
