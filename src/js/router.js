import Fabric from "./components/Builder/Tabs/Fabric.vue";
import Front from "./components/Builder/Tabs/Front.vue";
import Cuffs from "./components/Builder/Tabs/Cuffs.vue";
import Size from "./components/Builder/Tabs/Size.vue";

import Neck from "./components/Builder/Tabs/Neck.vue";
import BodyShape from "./components/Builder/Tabs/BodyShape.vue";
import Fit from "./components/Builder/Tabs/Fit.vue";
import Sleeves from "./components/Builder/Tabs/Sleeves.vue";
import Shoulders from "./components/Builder/Tabs/Shoulders.vue";

import Measurements from "./components/Builder/Tabs/SizeMeasurements.vue";
import MySizes from "./components/Builder/Tabs/MySizes.vue";
import Results from "./components/Builder/Tabs/ShirtBuilding.vue";

export const routes = [
  { path: "/", component: Results, name: "main" },
  { path: "/fabric", name: "fabric", component: Fabric },
  { path: "/front", name: "front", component: Front },
  { path: "/cuffs", name: "cuffs", component: Cuffs },
  { path: "/size", name: "size", component: Size },
  { path: "/size/neck", component: Neck },
  { path: "/size/body-shape", component: BodyShape, name: "body-shape" },
  { path: "/size/fit", component: Fit },
  { path: "/size/sleeves", component: Sleeves },
  { path: "/size/shoulders", component: Shoulders },
  { path: "/size/measurements", component: Measurements },
  { path: "/sizes/:size?", component: MySizes, name: "sizes" },
  { path: "*", redirect: "/" }
];
