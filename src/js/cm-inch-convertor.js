Number.prototype.round = function(places) {
    return +(Math.round(this + "e+" + places)  + "e-" + places);
  }
export default {
    data: function() {
        return {
            activeMeasure: 'inches',
        }
    },
    computed: {
        clean_measures() {
            return JSON.parse(JSON.stringify(this.measures));
        },
        cm_measures() {
          if (this.activeMeasure == 'cm') {
            return this.currentMeasures
          }
          const cm_measures = {};
            const measures_keys = Object.keys(this.currentMeasures);

            measures_keys.forEach((element, index) => {
                const arr = this.currentMeasures[element];

                if (element == 'shoulders') {
                    cm_measures[element] = arr;
                  } else {
                      cm_measures[element] = [];
                      for (let i = 0, iLen = arr.length; i < iLen; i++) {
                        const el = arr[i];
                        cm_measures[element].push({name: el.name, value: (el.value / 0.39370).round(2)});
                      }
                  }
            })
            return cm_measures
        },
        inch_measures() {
          if (this.activeMeasure == 'inches') {
            return this.currentMeasures
          }
          const inch_measures = {};
          const measures_keys = Object.keys(this.currentMeasures);

          measures_keys.forEach((element, index) => {
              const arr = this.currentMeasures[element];

              if (element == 'shoulders') {
                  inch_measures[element] = arr;
                } else {
                    inch_measures[element] = [];
                    for (let i = 0, iLen = arr.length; i < iLen; i++) {
                      const el = arr[i];
                      inch_measures[element].push({name: el.name, value: (el.value * 0.39370).round(2)});
                    }
                }
          })
          return inch_measures
        }
    },
    methods: {
        toINCH() {
            this.currentMeasures = this.inch_measures
            this.setActiveMeasure('inches');
        },
        toCM() {
            this.currentMeasures = this.cm_measures
            this.setActiveMeasure('cm');
        },
        isActiveMeasure: function (measure) {
            return this.activeMeasure === measure
        },
        setActiveMeasure: function (measure) {
            this.activeMeasure = measure
        },
    }
}
