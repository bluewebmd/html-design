

jQuery(document).ready(function ($) {

  $(".section_options").on("click", ".size__button-delete", function (e) {
    e.preventDefault();
    $.ajax({
      url: main_ajax_object.ajax_url,
      type: "GET",
      data: {
        action: "delete_size",
        nonce: $(this).data("nonce"),
        size_name: $(this).data("size_name")
      },
      dataType: "json",
      beforeSend: function () {
        // $(".size__buttons").prop("disabled", true);
        // $(".sizes").css({
        //   "-webkit-animation": "image_blur_in 3s"
        // });
        $("body").css("cursor", "progress");

      },
      success: function (res) {
        if (res.success) {
          $(".sizes").remove();
          $("#saved-sizes").after(JSON.parse(res.data.html));


        } else {
          $(".sizes:first-child").prepend(
            '<p class="notification notification-error">' +
            res.data +
            "</p>"
          );
        }
        $(".size__buttons").prop("disabled", false);
        $("body").css("cursor", "default");
        // $(".sizes").css({
        //   "-webkit-animation": "image_blur_out 1s"
        // });
      }
    });

    return false;
  });


  $(".collection__item").on("change", "select", function(e) {
    e.preventDefault();
    const $this = $(this);
    const $item = $this
      .parent()
      .parent()
      .parent();
    const $folder = profile_object.img_path + "shirts/";
    const $ext = ".png";
    const $fabric = $item.find(".collection__item__data__chosen-fabric").data("id");
    const $front = $item.find("select[name=front]").val();
    const $cuffs = $item.find("select[name=cuffs]").val();
    let $fileName = $fabric + " - " + $cuffs + " - " + $front;

    let cuffsUrl = $fileName.split(" - ");
    cuffsUrl[1] = $cuffs
      .split(" ")[0]
      .toLowerCase()
      .replace(" ", "-");
    $fileName = cuffsUrl.join(" - ");

    let frontUrl = $fileName.split(" - ");
    frontUrl[2] = $front.toLowerCase().replace(" ", "-");
    $fileName = frontUrl.join(" - ");

    const $newImg = $folder + $fileName + $ext;

    $item.find(".collection__item__image").attr("src", $newImg);

    return false;
  });

  $(".collection__items-collections").on("click", ".collection__item__data__add-to-cart", function (e) {
    e.preventDefault();
    const $this = $(this);
    const $item = $this
      .parent()
      .parent();

    const $fabric = $item.find(".collection__item__data__chosen-fabric").text();
    const $fabric_type = $item.find(".collection__item__data__chosen-fabric").data("type");
    const $fabric_id = $item.find(".collection__item__data__chosen-fabric").data("id");
    const $front = $item.find("select[name=front]").val();
    const $cuffs = $item.find("select[name=cuffs]").val();
    const $size = $item.find("select[name=size]").val();

    $.ajax({
      url: main_ajax_object.ajax_url,
      type: "POST",
      data: {
        action: "add_collection_to_cart",
        nonce: profile_object.nonce,
        fabric: $fabric,
        fabric_id: $fabric_id,
        fabric_type: $fabric_type,
        front: $front,
        cuffs: $cuffs,
        size: $size,
      },
      dataType: "json",
      beforeSend: function() {
        $this.prop("disabled", true);
        $item.css({
          "-webkit-animation": "image_blur_in 3s"
        });
      },
      success: function(res) {
        if (res.success) {
          $(".cart__items").text(res.data.quantity);
          $(".mini-cart").html(JSON.parse(res.data.html));
        } else {
          alert(res.data);
        }
        $this.prop("disabled", false);
        $item.css({
          "-webkit-animation": "image_blur_out 1s"
        });
      }
    });

    return false;
  });

  $(".collection__items-history").on("click", ".collection__item__data__add-to-cart", function (e) {
    e.preventDefault();
    const $this = $(this);
    const $item = $this
      .parent()
      .parent();

    const $fabric = $item
      .find(".collection__item__data__chosen-fabric")
      .text();
    const $fabric_type = $item
      .find(".collection__item__data__chosen-fabric")
      .data("type");
    const $fabric_id = $item
      .find(".collection__item__data__chosen-fabric")
      .data("id");
    const $front = $item
      .find(".collection__item__data__chosen-front")
      .text();
    const $cuffs = $item
      .find(".collection__item__data__chosen-cuffs")
      .text();
    const $size = $item
      .find(".collection__item__data__chosen-size_name")
      .text();

    $.ajax({
      url: main_ajax_object.ajax_url,
      type: "POST",
      data: {
        action: "add_to_cart_from_history",
        nonce: profile_object.nonce,
        fabric: $fabric,
        fabric_id: $fabric_id,
        fabric_type: $fabric_type,
        front: $front,
        cuffs: $cuffs,
        size: $size,
      },
      dataType: "json",
      beforeSend: function() {
        $this.prop("disabled", true);
        $item.css({
          "-webkit-animation": "image_blur_in 3s"
        });
      },
      success: function(res) {
        if (res.success) {
          $(".cart__items").text(res.data.quantity);
          $(".mini-cart").html(JSON.parse(res.data.html));
        } else {
          alert(res.data);
        }
        $this.prop("disabled", false);
        $item.css({
          "-webkit-animation": "image_blur_out 1s"
        });
      }
    });

    return false;
  });

});
