let profileSubmenu = (function($) {
  let init = function() {
    setUpListeners();
  };

  let setUpListeners = function() {
    $(".profile__menu-button").click(function(e) {
      if ($(window).width() < 701) {
        $(this).toggleClass("active");
        $(".profile__menu-list")
          .stop()
          .slideToggle();
      }
    });
  };

  return {
    init: init
  };
})(jQuery);

module.exports = {
  profileSubmenu
};
