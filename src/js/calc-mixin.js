import measures from "./components/Builder/store/measureSheet";

export default {
  computed: {
    currentMeasures() {
      return this.$store.getters.getMeasures;
    },
    fit() {
      return this.$store.getters.getFit;
    },
    body() {
      const body = this.$store.getters.getBody;

      switch (body) {
        case 3:
          return "medium";
        case 4:
          return "significant";
        default:
          return "default";
      }
    },
    neck() {
      return this.$store.getters.getNeck;
    },
    mHeight() {
      const height = this.$store.getters.getHeight;
      return height > 175 ? "more" : "less";
    },
    shoulders() {
      return this.$store.getters.getShoulders;
    },
    sleeves() {
      return this.$store.state.parts.size.customSleeves
        ? this.$store.state.parts.size.customSleeves
        : measures[this.fit][this.neck].sleeve[this.mHeight];
    }
  },
  methods: {
    setMeasures() {
      this.$store.commit("updateMeasures", {
        body: [
          {
            name: "Body Length",
            value: measures[this.fit][this.neck].length[this.mHeight]
          },
          { name: "Shoulder", value: measures[this.fit][this.neck].shoulder },
          { name: "Chest circumference", value: measures[this.fit][this.neck].chest },
          {
            name: "Waist circumference",
            value: measures[this.fit][this.neck].waist[this.body]
          },
          { name: "Hips circumference", value: measures[this.fit][this.neck].hips }
        ],
        neck: [{ name: "Neck Circumference", value: this.neck }],
        sleeves: [
          { name: "Sleeve", value: this.sleeves },
          { name: "Elbow circumference", value: measures[this.fit][this.neck].elbow },
          { name: "Cuffs circumference", value: measures[this.fit][this.neck].cuffs }
        ],
        shoulders: this.shoulders
      });
    }
  }
};
