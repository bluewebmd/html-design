const webpack = require("webpack");
const vendor = require("./package.json");
const path = require("path");
const ETP = require("extract-text-webpack-plugin");
const inProduction = process.env.NODE_ENV === "production";

const BrowserSync = require("browser-sync-webpack-plugin");

module.exports = {
  entry: {
    app: "./src/main.js",
    builder: ["./src/builder.js", "./src/sass/pages/_shirt-builder.sass"],
    vendor: Object.keys(vendor.dependencies),
    home: "./src/sass/pages/_home.sass",
    about: "./src/sass/pages/_about-us.sass",
    shirt_details: "./src/sass/pages/_shirt-details.sass",
    how_it_works: "./src/sass/pages/_how-it-works.sass",
    coming_soon: "./src/sass/pages/_coming-soon.sass",
    order_history: "./src/sass/pages/_order-history.sass",
    big_basket: ["./src/sass/pages/_big-basket.sass", "./src/js/builder.js"],
    profile: ["./src/sass/pages/_profile.sass"],
    checkout: "./src/sass/pages/_checkout.sass",
    general_info: "./src/sass/pages/_general-info.sass",
    giftcard: "./src/sass/pages/_giftcard.sass",
    payment_method: "./src/sass/pages/_payment-method.sass",
    blog: "./src/sass/pages/_blog.sass",
    single: "./src/sass/pages/_blog-single.sass"
  },
  output: {
    path: path.resolve(__dirname, "./dist"),
    publicPath: "/dist/",
    filename: "[name].js"
  },
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: "vue-loader",
        options: {
          loaders: {
            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this necessary.
            scss: "vue-style-loader!css-loader!sass-loader",
            sass: "vue-style-loader!css-loader!sass-loader?indentedSyntax"
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.(css|sass)$/,
        use: ETP.extract({
          fallback: "style-loader",
          use: [
            {
              loader: "css-loader",
              options: {
                url: false,
                sourceMap: true
              }
            },
            {
              loader: "postcss-loader",
              options: {
                sourceMap: "inline"
              }
            },
            {
              loader: "sass-loader",
              options: {
                sourceMap: true
              }
            }
          ]
        })
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      }
    ]
  },
  resolve: {
    alias: {
      vue$: "vue/dist/vue.esm.js"
    }
  },

  plugins: [
    new ETP("[name].css"),
    // new webpack.LoaderOptionsPlugin({
    //   minimize: inProduction
    // }),
    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",
      filename: "vendor.js"
    }),
    new webpack.optimize.MinChunkSizePlugin({
      minChunkSize: 10000 // Minimum number of characters
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      Vue: ["vue/dist/vue.esm.js", "default"]
    })
  ]
};

if (process.env.NODE_ENV === "dev") {
  module.exports.plugins.push(
    new BrowserSync({
      host: "localhost",
      port: 3000,
      proxy: "localhost:8081"
    })
  );
}

if (process.env.NODE_ENV === "production") {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    })
    // new webpack.DefinePlugin({
    //   "process.env.NODE_ENV": JSON.stringify("production")
    // })
  );
}
